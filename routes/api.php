<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Grouped requests with CORS enabled
Route::group([], function () {

    /****************************** POIs ******************************/               
    Route::resource('pois', 'PoiController');
    Route::get('pois/search/{search}', 'PoiController@searchPois');
    Route::get('pois/details/types', 'PoiController@getDetailTypes');

    /****************************** Places ****************************/  
    Route::get('places/liked', 'PlaceController@placesMostLiked');
    Route::resource('places', 'PlaceController');
    Route::get('places/pois/{url}', 'PlaceController@placeWithPois');
    Route::post('places/pois/{url}', 'PlaceController@searchPoisFromCity');
    Route::get('places/cities/{cities}', 'PlaceController@findCities');
    Route::get('places/states/{states}', 'PlaceController@findStates');
    Route::get('places/countries/{countries}', 'PlaceController@findCountries');
    Route::get('places/all/{search}', 'PlaceController@findAll');
    Route::get('places/categories/{url}', 'PlaceController@placeWithCategories');

    /***************************** Categories *************************/
    Route::resource('categories', 'CategoryController');

    /***************************** Users ******************************/
    Route::get('users/{username}', 'UserController@getUserByUsername');

    /***************************** Login / SignUp *********************/
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('signup', 'AuthController@signup');
      
        /***************************** Requires Login *****************/
        Route::group(['middleware' => 'auth:api'], function() {
            Route::get('logout', 'AuthController@logout');
            Route::get('user', 'AuthController@user');
        });
    });

    /****************************** Requires Login ********************/
    Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function() {
        /************* User Info ************/
        Route::get('profile', 'UserController@getPrivateProfile');
        Route::post('profile', 'UserController@editUserProfile');

        /************ User Suggestion *******/
        Route::post('suggestion', 'SuggestionController@store');
        Route::get('suggestions', 'SuggestionController@index');
        Route::put('suggestions/accept/{suggestionId}', 'SuggestionController@accept');
        Route::put('suggestions/deny/{suggestionId}', 'SuggestionController@deny');

        /************* User POIs ************/
        Route::post('pois', 'UserController@userLikesPoi');
        Route::delete('pois/{id}', 'UserController@userUnlikesPoi');
        Route::get('pois', 'UserController@retrieveLikedPois');

        /*********** User travel ************/
        Route::post('travels', 'UserController@addTravel');
        Route::get('travels', 'UserController@getTravels');
        Route::get('travels/visited-pois', 'UserController@visitedPois');
        Route::get('travels/{travelId}', 'UserController@getTravel');
        Route::put('travels/{travelId}', 'UserController@editTravel');
        Route::delete('travels/{travelId}', 'UserController@deleteTravel');

        /*********** User Reviews ***********/
        Route::post('reviews', 'ReviewController@store');
        Route::get('reviews', 'ReviewController@userReviews');
        Route::delete('reviews/{reviewId}', 'ReviewController@deleteReview');

        /************ Find Users ************/
        Route::get('users/places/{place}', 'UserController@getUsersByPlace');
        Route::get('users/username/{username}', 'UserController@getUsersByUsername');
        
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'auth:api'], function() {
        /*********** User Reviews ***********/
        Route::get('reviews', 'ReviewController@getPendingReviews');
        Route::get('reviews/approve/{reviewId}', 'ReviewController@approveReview');
        Route::get('reviews/deny/{reviewId}', 'ReviewController@denyReview');

        /*********** Manage Places **********/
        Route::post('place', 'PlaceController@adminStore');
        Route::delete('places/{placeId}', 'PlaceController@destroy');
        Route::post('places/{placeId}', 'PlaceController@edit');
        Route::get('places/{placeId}', 'PlaceController@getPlace');

        /*********** User Most Liked POIs ***********/
        Route::get('pois/liked', 'PoiController@usersLikedPois');

        /*********** Manage Pois ************/
        Route::post('poi', 'PoiController@adminStore');
        Route::delete('pois/{poiId}', 'PoiController@destroy');
        Route::post('pois/{poiId}', 'PoiController@edit');
        Route::get('pois/{poiId}', 'PoiController@getPoi');
        Route::get('pois/categories/{poiId}', 'PoiController@getPoiCategories');
    });
});