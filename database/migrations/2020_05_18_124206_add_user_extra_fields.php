<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('lastname')->after('name')->nullable();
            $table->string('username')->after('lastname')->nullable()->unique();
            $table->date('birthday')->after('username')->nullable();
            $table->longText('description')->after('birthday')->nullable();
            $table->boolean('is_private')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('lastname');
            $table->dropColumn('username');
            $table->dropColumn('birthday');
            $table->dropColumn('description');
            $table->dropColumn('is_private');
        });
    }
}
