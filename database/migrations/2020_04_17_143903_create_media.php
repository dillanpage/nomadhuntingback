<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->unsignedInteger('review_id')->nullable();
            $table->unsignedInteger('poi_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('place_id')->nullable();
            $table->timestamps();

            // Add foreign keys
            $table->foreign('review_id')->references('id')->on('reviews');
            $table->foreign('poi_id')->references('id')->on('pois');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('place_id')->references('id')->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
