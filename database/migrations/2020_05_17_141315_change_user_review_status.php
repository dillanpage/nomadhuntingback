<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserReviewStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->enum('status', ['approved','denied','pending'])->default('pending')->after('place_id');
        });

        Schema::table('reviews', function($table) {
            $table->dropColumn('approved');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function($table) {
            $table->dropColumn('status');
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->boolean('approved')->after('place_id')->default(0);
        });
    }
}
