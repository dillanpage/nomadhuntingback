<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropForeign('media_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
        });

        Schema::table('pois', function (Blueprint $table) {
            $table->dropForeign('pois_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->dropForeign('reviews_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
        });

        Schema::table('travels', function (Blueprint $table) {
            $table->dropForeign('travels_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
