<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPoiDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pois', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['-','address','admission','email','phone','opening_hours'])->nullable();
            $table->string('type_es')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('poi_id');
            $table->foreign('poi_id')->references('id')->on('pois');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pois');
    }
}
