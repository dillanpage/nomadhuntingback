<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->tinyInteger('rating');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('poi_id');
            $table->unsignedInteger('place_id');
            $table->timestamps();

            // Add foreign keys
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('poi_id')->references('id')->on('pois');
            $table->foreign('place_id')->references('id')->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
