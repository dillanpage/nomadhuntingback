<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sygic_id')->nullable();
            $table->string('level');
            $table->string('name');
            $table->string('url')->unique();
            $table->string('name_local')->nullable();
            $table->string('name_en')->nullable();
            $table->unsignedInteger('parent_continent')->nullable();
            $table->unsignedInteger('parent_country')->nullable();
            $table->unsignedInteger('parent_state')->nullable();
            $table->unsignedInteger('parent_region')->nullable();
            $table->unsignedInteger('parent_city')->nullable();
            $table->text('description')->nullable();
            $table->string('thumbnail_url')->nullable();
            $table->string('image_url')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('place_id')->references('id')->on('places');
        });

        Schema::table('pois', function (Blueprint $table) {
            $table->foreign('place_id')->references('id')->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['place_id']);
        });

        Schema::table('pois', function (Blueprint $table) {
            $table->dropForeign(['place_id']);
        });
        
        Schema::dropIfExists('places');
    }
}
