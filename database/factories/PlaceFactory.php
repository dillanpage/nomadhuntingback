<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Place;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Place::class, function (Faker $faker) use ($factory) {
    return [
        'level' => $faker->name,
        'name' => $faker->name,
        'name_local' => $faker->name,
        'name_en' => $faker->name,
        'url' => $faker->unique()->url,
        'description' => $faker->name,
        'thumbnail_url' => $faker->url,
        'image_url' => $faker->url
    ];
});
