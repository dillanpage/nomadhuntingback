<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DetailPoi;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(DetailPoi::class, function (Faker $faker) use ($factory) {
    return [
        'type' => '-',
        'description' => $faker->realText(),
        'poi_id' => $factory->create(App\Poi::class)->id
    ];
});
