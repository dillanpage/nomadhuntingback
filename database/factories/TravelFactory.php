<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Travel;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Travel::class, function (Faker $faker) use ($factory) {
    $start_date = Carbon::now()->addDays(rand(10,100));
    $end_date = $start_date->addDays(rand(10,100));  

    return [
        'start_date' => $start_date->format('Y-m-d'),
        'end_date' => $end_date->format('Y-m-d'),
        'user_id' => $factory->create(App\User::class)->id,
        'place_id' => $factory->create(App\Place::class)->id
    ];
});
