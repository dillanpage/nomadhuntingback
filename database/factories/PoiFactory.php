<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Poi;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Poi::class, function (Faker $faker) use ($factory) {
    return [
        'name' => $faker->name,
        'url' => $faker->name,
        'description' => $faker->name,
        'thumbnail_url' => $faker->url,
        'image_url' => $faker->url,
        'place_id' => $factory->create(App\Place::class)->id
    ];
});
