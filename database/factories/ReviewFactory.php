<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$poiIds = [6366,6367,6368,6369,6370,6371,6372,6373,6374,6375,6376,6377,6378,6379,6380,6381,6382,6383,6384,6385,6386,6387,6388,6389,6390,6391,6392,6393,6394,6395,6396,6397,6398,6399,6400,6401,6402,6403,6404,6405,6406,6407,6408,6409,6410,6411,6412,6413,6414,6415,6416,6417,6418,6419,6420,6421,6422,6423,6424,6425,6426,6427,6428,6429,6430,6431];
$userIds = [3,7,8,9,10,11,12,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,1,5,6,13];

$factory->define(Review::class, function (Faker $faker) use ($factory, $poiIds, $userIds) {
    return [
        'description' => $faker->realText(),
        'rating' => $faker->numberBetween(2, 5),
        'user_id' => $faker->randomElement($userIds),
        'poi_id' => $faker->randomElement($poiIds),
        'status' => 'approved',
    ];
});
