<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\TagsNotAllowed;

$factory->define(TagsNotAllowed::class, function (Faker $faker) use ($factory) {
    return [
        'name' => $faker->name
    ];
});
