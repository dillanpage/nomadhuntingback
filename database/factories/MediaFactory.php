<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Media;
use Faker\Generator as Faker;

$factory->define(Media::class, function (Faker $faker) use ($factory) {
    return [
        'url' => $faker->url,
        'user_id' => $factory->create(App\User::class)->id,
        'poi_id' => $factory->create(App\Poi::class)->id
    ];
});
