<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Transporte',
                'name_en' => 'traveling',
                'url' => 'transporte'
            ],
            [
                'name' => 'Compras',
                'name_en' => 'shopping',
                'url' => 'compras'
            ],
            [
                'name' => 'Deportes',
                'name_en' => 'sports',
                'url' => 'deportes'
            ],
            [
                'name' => 'Restaurantes',
                'name_en' => 'eating',
                'url' => 'restaurantes'
            ],
            [
                'name' => 'Aire Libre',
                'name_en' => 'hiking',
                'url' => 'aire-libre'
            ],
            [
                'name' => 'Alojamiento',
                'name_en' => 'sleeping',
                'url' => 'alojamiento'
            ],
            [
                'name' => 'Relajación',
                'name_en' => 'relaxing',
                'url' => 'relajacion'
            ],
            [
                'name' => 'Familia',
                'name_en' => 'playing',
                'url' => 'familia'
            ],
            [
                'name' => 'Vida Nocturna',
                'name_en' => 'going_out',
                'url' => 'vida-nocturna'
            ],
            [
                'name' => 'Atracciones turísticas',
                'name_en' => 'sightseeing',
                'url' => 'atracciones-turisticas'
            ],
            [
                'name' => 'Museos',
                'name_en' => 'discovering',
                'url' => 'museos'
            ]
        ];

        // DB::table('categories')->insert($categories);
    }
}
