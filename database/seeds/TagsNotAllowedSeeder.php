<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsNotAllowedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags_not_allowed = [
            ["name" => "Archipelago"],
            ["name" => "Borough, City Block, District, Neighborhood, Quarter"],
            ["name" => "City"],
            ["name" => "Country"],
            ["name" => "County"],
            ["name" => "Island"],
            ["name" => "Municipality"],
            ["name" => "Region"],
            ["name" => "State"],
            ["name" => "Suburb"],
            ["name" => "Town"],
            ["name" => "Village"],
            ["name" => "Starbucks"],
            ["name" => "Burger King"],
            ["name" => "Fast Food"],
            ["name" => "Bagels"],
            ["name" => "Baguette"],
            ["name" => "Döner"],
            ["name" => "Falafel"],
            ["name" => "Fries"],
            ["name" => "Gyros"],
            ["name" => "Hot Dog Stand"],
            ["name" => "Kebap"],
            ["name" => "KFC"],
            ["name" => "McDonald's"],
            ["name" => "Sandwiches Place"],
            ["name" => "Subway"],
            ["name" => "Sausages"],
            ["name" => "Snacks"],
            ["name" => "Tacos"],
            ["name" => "Taco Bell"],
            ["name" => "Pizza Hut"],
            ["name" => "Cabaret"],
            ["name" => "Casino"],
            ["name" => "Gambling"],
            ["name" => "Strip Club"],
            ["name" => "Swinger Club"],
            ["name" => "Animal Shelter"],
            ["name" => "Animals"],
            ["name" => "ATM"],
            ["name" => "Band Stand"],
            ["name" => "Bank"],
            ["name" => "Currency Exchange"],
            ["name" => "Bench"],
            ["name" => "Farm"],
            ["name" => "Barn"],
            ["name" => "Gazebo"],
            ["name" => "Greenhouse"],
            ["name" => "Community Hall"],
            ["name" => "Exhibition Hall"],
            ["name" => "Village Hall"],
            ["name" => "Manor House"],
            ["name" => "Passage"],
            ["name" => "Clock"],
            ["name" => "Doctor"],
            ["name" => "Clinic"],
            ["name" => "Dentist"],
            ["name" => "Hospital"],
            ["name" => "Health Resort"],
            ["name" => "Physician"],
            ["name" => "Physiotherapist"],
            ["name" => "Therapist"],
            ["name" => "Veterinary"],
            ["name" => "Water Tap"],
            ["name" => "Emergency Service"],
            ["name" => "Ambulance Station"],
            ["name" => "Defibrillator"],
            ["name" => "Emergency Evacuation Centre"],
            ["name" => "Fire Station"],
            ["name" => "Lifeguard Base"],
            ["name" => "Mountain Rescue"],
            ["name" => "Police Station"],
            ["name" => "Ranger Station"],
            ["name" => "Water Rescue Station"],
            ["name" => "Factory"],
            ["name" => "Sawmill"],
            ["name" => "Waste Treatment"],
            ["name" => "Water Plant"],
            ["name" => "Waste Water Plant"],
            ["name" => "Funeral Services"],
            ["name" => "Gate"],
            ["name" => "Hacker Space"],
            ["name" => "Hairdresser"],
            ["name" => "Laundry"],
            ["name" => "Dry Cleaning"],
            ["name" => "Military"],
            ["name" => "Military Airfield"],
            ["name" => "Military Office"],
            ["name" => "Office"],
            ["name" => "Advertising Agency"],
            ["name" => "Company Office"],
            ["name" => "Driving School"],
            ["name" => "Employment Service"],
            ["name" => "Foundation Office"],
            ["name" => "Notary Public"],
            ["name" => "Tax Office"],
            ["name" => "Insurance Office"],
            ["name" => "IT Specialist"],
            ["name" => "Lawyer"],
            ["name" => "Logistics Company"],
            ["name" => "Newspaper Office"],
            ["name" => "Non-government Organisation"],
            ["name" => "R&D Office"],
            ["name" => "Studio"],
            ["name" => "Tax Advisor"],
            ["name" => "Telecommunications Company"],
            ["name" => "Pharmacy"],
            ["name" => "Pier"],
            ["name" => "Post Box"],
            ["name" => "Post Office"],
            ["name" => "Deutsche Post"],
            ["name" => "Energy Supplier"],
            ["name" => "Power Supply"],
            ["name" => "Prison"],
            ["name" => "Recreational Area"],
            ["name" => "Recycling"],
            ["name" => "Recycling Centre"],
            ["name" => "School"],
            ["name" => "Childcare"],
            ["name" => "Dancing School"],
            ["name" => "Educational Institution"],
            ["name" => "Kindergarten"],
            ["name" => "Language School"],
            ["name" => "Music School"],
            ["name" => "Primary School"],
            ["name" => "Secondary School"],
            ["name" => "Settlement"],
            ["name" => "Shelter"],
            ["name" => "Shower"],
            ["name" => "Social Facility"],
            ["name" => "Assisted Living"],
            ["name" => "Nursing Home"],
            ["name" => "Outreach"],
            ["name" => "Retirement Home"],
            ["name" => "Youth Centre"],
            ["name" => "Square"],
            ["name" => "Steps"],
            ["name" => "Storage Rental"],
            ["name" => "Toilets"],
            ["name" => "Latrine"],
            ["name" => "Sanitary Dump Station"],
            ["name" => "Travel Agency"],
            ["name" => "Canal"],
            ["name" => "Dam"],
            ["name" => "Pond"],
            ["name" => "Reef"],
            ["name" => "Reservoir"],
            ["name" => "River"],
            ["name" => "Well"],
            ["name" => "Family Place"],
            ["name" => "Amusement Arcade"],
            ["name" => "Video Games"],
            ["name" => "Carousel"],
            ["name" => "Playground"],
            ["name" => "Balance Beam"],
            ["name" => "Climbing Wall"],
            ["name" => "Indoor Playground"],
            ["name" => "Play House"],
            ["name" => "Sand Pit"],
            ["name" => "Slide"],
            ["name" => "Play Structure"],
            ["name" => "Massage"],
            ["name" => "Thai Massage"],
            ["name" => "Sauna"],
            ["name" => "Spa"],
            ["name" => "Baths"],
            ["name" => "Wellness"],
            ["name" => "Art Shop"],
            ["name" => "Antiques Shop"],
            ["name" => "Frame Shop"],
            ["name" => "Baby Goods Store"],
            ["name" => "Bag Shop"],
            ["name" => "Cosmetics Shop"],
            ["name" => "Perfumery"],
            ["name" => "Tanning Salon"],
            ["name" => "Beekeeper"],
            ["name" => "Beverages Shop"],
            ["name" => "Car Dealership"],
            ["name" => "Motorcycle Shop"],
            ["name" => "Car Parts Shop"],
            ["name" => "Tyres Shop"],
            ["name" => "Department Store"],
            ["name" => "Chemist"],
            ["name" => "Clothes"],
            ["name" => "Baby Clothes"],
            ["name" => "Boutique"],
            ["name" => "Children Clothes"],
            ["name" => "Denim Clothes"],
            ["name" => "Dress Maker"],
            ["name" => "Fabric Shop"],
            ["name" => "Fashion Store"],
            ["name" => "Hats Shop"],
            ["name" => "Leather Store"],
            ["name" => "Men Clothes"],
            ["name" => "Sports Clothes"],
            ["name" => "Tailor"],
            ["name" => "Underwear"],
            ["name" => "Lingerie Shop"],
            ["name" => "Wedding Clothes"],
            ["name" => "Women Clothes"],
            ["name" => "Workwear"],
            ["name" => "Confectionery"],
            ["name" => "Convenience Store"],
            ["name" => "Copy Shop"],
            ["name" => "Craft Shop"],
            ["name" => "Builder"],
            ["name" => "Carpenter"],
            ["name" => "Caterer"],
            ["name" => "Electrician"],
            ["name" => "Electronics Repair"],
            ["name" => "Gardener"],
            ["name" => "Glaziery"],
            ["name" => "HVAC"],
            ["name" => "Joiner"],
            ["name" => "Key Cutter"],
            ["name" => "Metal Construction"],
            ["name" => "Painter"],
            ["name" => "Plumber"],
            ["name" => "Roofer"],
            ["name" => "Shoe Repair Shop"],
            ["name" => "Stonemason"],
            ["name" => "Tiler"],
            ["name" => "Upholsterer"],
            ["name" => "Window Construction"],
            ["name" => "Crafts Shop"],
            ["name" => "Dairy Shop"],
            ["name" => "Cheese Shop"],
            ["name" => "Deli"],
            ["name" => "Candy"],
            ["name" => "Chocolate"],
            ["name" => "Greengrocer"],
            ["name" => "Pastry"],
            ["name" => "Seafood Shop"],
            ["name" => "Nutrition Supplements Shop"],
            ["name" => "Wholefoods"],
            ["name" => "Discount Store"],
            ["name" => "Do it Yourself Store"],
            ["name" => "Hardware Store"],
            ["name" => "Blacksmith"],
            ["name" => "Interior Decoration"],
            ["name" => "Paint Shop"],
            ["name" => "Electrical Shop"],
            ["name" => "Electronics"],
            ["name" => "Apple"],
            ["name" => "Appliances Shop"],
            ["name" => "Computer Store"],
            ["name" => "Hearing Aids"],
            ["name" => "HiFi Shop"],
            ["name" => "Mobile Phones Shop"],
            ["name" => "Radiotechnics Shop"],
            ["name" => "Video Shop"],
            ["name" => "Erotic Shop"],
            ["name" => "Estate Agent"],
            ["name" => "Fishing Equipment"],
            ["name" => "Florist"],
            ["name" => "Game Shop"],
            ["name" => "Gas Shop"],
            ["name" => "General Store"],
            ["name" => "Gift"],
            ["name" => "Handicraft"],
            ["name" => "Bathroom Furnishing Shop"],
            ["name" => "Bed Shop"],
            ["name" => "Carpet Shop"],
            ["name" => "Curtain Shop"],
            ["name" => "Flooring Shop"],
            ["name" => "Furniture Store"],
            ["name" => "Garden Centre"],
            ["name" => "Houseware Shop"],
            ["name" => "Kitchen Studio"],
            ["name" => "Lighting Shop"],
            ["name" => "Ice Cream"],
            ["name" => "Jewellery"],
            ["name" => "Jeweller"],
            ["name" => "Kiosk"],
            ["name" => "Locksmith"],
            ["name" => "Medical Supply Shop"],
            ["name" => "Music Store"],
            ["name" => "Newsagent"],
            ["name" => "Optician"],
            ["name" => "Pawnbroker"],
            ["name" => "Pet Shop"],
            ["name" => "Herbalist"],
            ["name" => "Photo Shop"],
            ["name" => "Photo Studio"],
            ["name" => "Pottery"],
            ["name" => "Second Hand"],
            ["name" => "Charity Shop"],
            ["name" => "Sewing Shop"],
            ["name" => "Shoes"],
            ["name" => "Shoemaker"],
            ["name" => "Souvenir"],
            ["name" => "Sports Shop"],
            ["name" => "Outdoor Equipment Shop"],
            ["name" => "Scuba Diving Shop"],
            ["name" => "Ski Shop"],
            ["name" => "Ski Rental"],
            ["name" => "Stationery"],
            ["name" => "Shopping Street"],
            ["name" => "Supermarket"],
            ["name" => "Grocery"],
            ["name" => "Lidl"],
            ["name" => "Магнит"],
            ["name" => "Spar"],
            ["name" => "Tesco"],
            ["name" => "Tattoo Shop"],
            ["name" => "Ticket Shop"],
            ["name" => "Tobacco Shop"],
            ["name" => "E-cigarettes Shop"],
            ["name" => "Toys"],
            ["name" => "Lego"],
            ["name" => "Model Shop"],
            ["name" => "Watches Shop"],
            ["name" => "Watchmaker"],
            ["name" => "Weapons Shop"],
            ["name" => "Hunting Shop"],
            ["name" => "Wholesale"],
            ["name" => "Accommodation"],
            ["name" => "Apartment"],
            ["name" => "Aparthotel"],
            ["name" => "Bed and Breakfast"],
            ["name" => "Bungalow"],
            ["name" => "Chalet"],
            ["name" => "Guest Accommodation"],
            ["name" => "Holiday Home"],
            ["name" => "Home Stay"],
            ["name" => "Guest House"],
            ["name" => "Country House, Villa"],
            ["name" => "Residence"],
            ["name" => "Cabin"],
            ["name" => "Camp Site"],
            ["name" => "Backcountry Camping"],
            ["name" => "Caravan Site, Holiday Park"],
            ["name" => "Luxury Tent"],
            ["name" => "Scout Camp"],
            ["name" => "Summer Camp"],
            ["name" => "Tents"],
            ["name" => "Dormitory"],
            ["name" => "Farm Stay"],
            ["name" => "Hostel"],
            ["name" => "Hotel"],
            ["name" => "Boat Hotel"],
            ["name" => "Capsule Hotel"],
            ["name" => "Castle Hotel"],
            ["name" => "Countryside Hotel"],
            ["name" => "Design Hotel"],
            ["name" => "Eco-Friendly Hotel"],
            ["name" => "Budget Hotel, Economy Hotel"],
            ["name" => "Family Friendly Hotel"],
            ["name" => "Gourmet Hotel"],
            ["name" => "Inn"],
            ["name" => "Lodge"],
            ["name" => "Love Hotel"],
            ["name" => "Luxury Hotel"],
            ["name" => "Motel"],
            ["name" => "Resort"],
            ["name" => "Riad"],
            ["name" => "Ryokan"],
            ["name" => "Seaside Hotel"],
            ["name" => "Sport"],
            ["name" => "Archery Range"],
            ["name" => "Athletics"],
            ["name" => "Long Jump"],
            ["name" => "Running"],
            ["name" => "Shot Put"],
            ["name" => "Badminton"],
            ["name" => "Baseball"],
            ["name" => "Softball"],
            ["name" => "Bicycle Rental"],
            ["name" => "Billiard"],
            ["name" => "Snooker"],
            ["name" => "Bobsleigh"],
            ["name" => "Boules"],
            ["name" => "Bowling"],
            ["name" => "Bowls"],
            ["name" => "Box Ring"],
            ["name" => "Bullfighting"],
            ["name" => "Sports Centre"],
            ["name" => "Chess Table"],
            ["name" => "Circuit"],
            ["name" => "Climbing"],
            ["name" => "Rope Park"],
            ["name" => "Cock Fighting"],
            ["name" => "Cycling"],
            ["name" => "BMX Track"],
            ["name" => "Dancing"],
            ["name" => "Dog Racing"],
            ["name" => "Dog Training"],
            ["name" => "Exercise"],
            ["name" => "Fencing"],
            ["name" => "Sports Field"],
            ["name" => "Basketball"],
            ["name" => "Cricket"],
            ["name" => "Croquet Field"],
            ["name" => "Football"],
            ["name" => "Lacrosse"],
            ["name" => "Netball"],
            ["name" => "Rugby"],
            ["name" => "Soccer Field"],
            ["name" => "Futsal"],
            ["name" => "Volleyball"],
            ["name" => "Beach Volleyball"],
            ["name" => "Field Hockey"],
            ["name" => "Fishing"],
            ["name" => "Fishing Spot"],
            ["name" => "Fitness"],
            ["name" => "Fitness Name"],
            ["name" => "Fitness Centre"],
            ["name" => "Free Flying"],
            ["name" => "Gaelic Games"],
            ["name" => "Golf"],
            ["name" => "Disc Golf"],
            ["name" => "Minigolf"],
            ["name" => "Gym"],
            ["name" => "Gymnastics"],
            ["name" => "Handball"],
            ["name" => "Polo"],
            ["name" => "Horse Racing"],
            ["name" => "Horse Riding"],
            ["name" => "Stables"],
            ["name" => "Trail Riding Station"],
            ["name" => "Horseshoes"],
            ["name" => "Korfball"],
            ["name" => "Martial Arts"],
            ["name" => "Judo"],
            ["name" => "Karate"],
            ["name" => "Taekwondo"],
            ["name" => "Model Aerodrome"],
            ["name" => "Motor Sport"],
            ["name" => "Karting Track"],
            ["name" => "Motocross Track"],
            ["name" => "Motor Racing Track"],
            ["name" => "Orienteering"],
            ["name" => "Padel"],
            ["name" => "Paragliding"],
            ["name" => "Pelota"],
            ["name" => "Pétanque"],
            ["name" => "Pool"],
            ["name" => "Indoor Pool"],
            ["name" => "Outdoor Pool"],
            ["name" => "Public Pool"],
            ["name" => "Swimming Pool"],
            ["name" => "Racquet"],
            ["name" => "RC Cars Track"],
            ["name" => "Shooting Range"],
            ["name" => "Paint Ball"],
            ["name" => "Deck Shuffleboard"],
            ["name" => "Skateboard Park"],
            ["name" => "Skating"],
            ["name" => "Roller Skating"],
            ["name" => "Speedway"],
            ["name" => "Squash"],
            ["name" => "Stadium"],
            ["name" => "Table Soccer"],
            ["name" => "Tennis"],
            ["name" => "Mini tennis"],
            ["name" => "Paddle Tennis"],
            ["name" => "Table Tennis"],
            ["name" => "Toboggan"],
            ["name" => "Summer Toboggan"],
            ["name" => "Track"],
            ["name" => "Racetrack"],
            ["name" => "Trampoline"],
            ["name" => "Ultralight Aviation"],
            ["name" => "Water Sports"],
            ["name" => "Canoeing"],
            ["name" => "Diving"],
            ["name" => "Cliff Diving"],
            ["name" => "Scuba Diving"],
            ["name" => "Snorkeling"],
            ["name" => "Rowing"],
            ["name" => "Sailing"],
            ["name" => "Water Skiing"],
            ["name" => "Surfing"],
            ["name" => "Kite Surfing"],
            ["name" => "Swimming"],
            ["name" => "Winter Sports"],
            ["name" => "Curling"],
            ["name" => "Ice Stock"],
            ["name" => "Ice Hockey"],
            ["name" => "Ice Rink"],
            ["name" => "Skiing"],
            ["name" => "Cross Country Skiing"],
            ["name" => "Ski Jump"],
            ["name" => "Piste"],
            ["name" => "Advanced Piste"],
            ["name" => "Easy Piste"],
            ["name" => "Intermediate Piste"],
            ["name" => "Novice Piste"],
            ["name" => "Sleigh Run"],
            ["name" => "Snow Park"],
            ["name" => "Ski School"],
            ["name" => "Workout Spot"],
            ["name" => "Yoga"],
            ["name" => "Transport"],
            ["name" => "Airport"],
            ["name" => "Airfield"],
            ["name" => "Helipad"],
            ["name" => "International Airport"],
            ["name" => "Private Airport"],
            ["name" => "Airport Terminal"],
            ["name" => "Boat Rental"],
            ["name" => "Border Control"],
            ["name" => "Customs"],
            ["name" => "Cable Car"],
            ["name" => "Funicular"],
            ["name" => "Gondola Lift"],
            ["name" => "Chair Lift"],
            ["name" => "Drag Lift"],
            ["name" => "J-bar Lift"],
            ["name" => "Magic Carpet"],
            ["name" => "Poma Lift"],
            ["name" => "Rope Tow"],
            ["name" => "T-bar Lift"],
            ["name" => "Zip Line"],
            ["name" => "Cable Car Station"],
            ["name" => "Vehicle Inspection"],
            ["name" => "Car Rental"],
            ["name" => "Car Repair"],
            ["name" => "Car Sharing"],
            ["name" => "Car Wash"],
            ["name" => "Ferry Terminal"],
            ["name" => "Ferry Platform"],
            ["name" => "Car Charging"],
            ["name" => "Tesla Supercharger"],
            ["name" => "Gas Station"],
            ["name" => "Total"],
            ["name" => "Agip"],
            ["name" => "BP"],
            ["name" => "Enel"],
            ["name" => "Esso"],
            ["name" => "Lukoil"],
            ["name" => "MOL"],
            ["name" => "OMV"],
            ["name" => "Q8"],
            ["name" => "Repsol"],
            ["name" => "Shell"],
            ["name" => "Tamoil"],
            ["name" => "Parking"],
            ["name" => "Parking Garage"],
            ["name" => "Lane Parking"],
            ["name" => "Motorcycle Parking"],
            ["name" => "Multi Storey Parking"],
            ["name" => "Paid Parking"],
            ["name" => "Park and Ride"],
            ["name" => "Rooftop Parking"],
            ["name" => "Surface Parking"],
            ["name" => "Underground Parking"],
            ["name" => "Port"],
            ["name" => "Rest Area"],
            ["name" => "Rest Area"],
            ["name" => "Station"],
            ["name" => "Bus Station"],
            ["name" => "Bus Stop"],
            ["name" => "Trolleybus stop"],
            ["name" => "Lift Station"],
            ["name" => "Platform"],
            ["name" => "Stop"],
            ["name" => "Subway Stop"],
            ["name" => "Train Station"],
            ["name" => "Train Stop"],
            ["name" => "Tram Stop"],
            ["name" => "Taxi Stand"]
        ];

        DB::table('tags_not_allowed')->insert($tags_not_allowed);
    }
}
