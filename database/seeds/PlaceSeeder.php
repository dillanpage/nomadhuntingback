<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $continents = [
            [
                'level' => 'continent',
                'name' => 'Europa',
                'url' => 'europa',
                'sygic_id' => 'continent:1'
            ],
            [
                'level' => 'continent',
                'name' => 'América del Norte',
                'url' => 'america-del-norte',
                'sygic_id' => 'continent:6'
            ],
            [
                'level' => 'continent',
                'name' => 'América del Sur',
                'url' => 'america-del-sur',
                'sygic_id' => 'continent:7'
            ],
            [
                'level' => 'continent',
                'name' => 'África',
                'url' => 'africa',
                'sygic_id' => 'continent:2'
            ],
            [
                'level' => 'continent',
                'name' => 'Ásia',
                'url' => 'asia',
                'sygic_id' => 'continent:4'
            ],
            [
                'level' => 'continent',
                'name' => 'Oceanía',
                'url' => 'oceania',
                'sygic_id' => 'continent:5'
            ],
        ];

        // DB::table('places')->insert($continents);
    }
}
