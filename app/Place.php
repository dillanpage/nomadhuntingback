<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\DetailPoi;
use App\Poi;

class Place extends Model
{
    protected $fillable = ['sygic_id', 'level', 'name', 'name_local', 'name_en', 'description', 'thumbnail_url', 'image_url', 'latitude', 'longitude'];

    // Self relationship
    function continent(){
        return $this->belongsTo(Place::class, 'parent_continent');
    }

    // Self relationship
    function country(){
        return $this->belongsTo(Place::class, 'parent_country');
    }

    // Self relationship
    function state(){
        return $this->belongsTo(Place::class, 'parent_state');
    }

    // Self relationship
    function region(){
        return $this->belongsTo(Place::class, 'parent_region');
    }

    // Self relationship
    function city(){
        return $this->belongsTo(Place::class, 'parent_city');
    }

    // Relationship with POIS
    function pois(){
        return $this->hasMany(Poi::class, 'place_id');
    }

    public function filteredPoisContainingName($name)
    {
        return $this->hasMany(Poi::class)->where('name', 'LIKE',  '%'.$name.'%');
    }
}
