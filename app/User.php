<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'thumbnail_url', 'photo_url', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Get the place where this User belongs
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    // Get the travels of the User
    public function travels()
    {
        return $this->hasMany(Travel::class);
    }

    // Pivot table is user_poi
    public function pois()
    {
        return $this->belongsToMany(Poi::class);
    }

    // Check if User is Admin
    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function hasPublicProfile() {
        return $this->is_private == 0;
    }
}
