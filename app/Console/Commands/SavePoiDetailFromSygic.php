<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Place;
use App\Poi;
use App\Tag;
use App\DetailPoi;
use App\Category;
use App\Media;
use App\TagsNotAllowed;
use Exception;

class SavePoiDetailFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:poisExtraData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:poisExtraData");

        $pois = Poi::orderBy('id', 'asc')->get();

        $poiNameMapping = collect([
            'address' => 'Dirección',
            'admission' => 'Admisión',
            'email' => 'Email',
            'phone' => 'Teléfono',
            'opening_hours' => 'Horario'
        ]);

        foreach ($pois as $poi) {

            Log::info("POI ACTUAL: $poi->id");
            $client = new Client();

            // Set headers
            $headers['x-api-key'] = env("SYGIC_KEY", "api-key");

            try {
                // Send request to Sygic           
                $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/'.$poi->sygic_id.'', [
                    'headers' => $headers
                ]);

                $response = $request->getBody()->getContents();
                $response = json_decode($response, true);
                $response = collect($response['data']['place']);
            } catch (\Throwable $th) {
                throw new Exception("Error Processing Request");
            }

            $responsePoiDetails = [];

            Arr::get($response, 'address') ? $responsePoiDetails['address'] = Arr::get($response, 'address') : '';
            Arr::get($response, 'admission') ? $responsePoiDetails['admission'] = Arr::get($response, 'admission') : '';
            Arr::get($response, 'email') ? $responsePoiDetails['email'] = Arr::get($response, 'email') : '';
            Arr::get($response, 'phone') ? $responsePoiDetails['phone'] = Arr::get($response, 'phone') : '';
            Arr::get($response, 'opening_hours_note') ? $responsePoiDetails['opening_hours'] = Arr::get($response, 'opening_hours_note') : '';

                
            $detailsPoi = DetailPoi::where('poi_id', $poi->id)->count();
            // If we don't have any detail for this poi
            if ($detailsPoi == 0) {

                // Save all detail poi.
                foreach ($responsePoiDetails as $key => $value) {
                    $detailPoiModel = new DetailPoi();
                
                    $detailPoiModel->type = $key;
                    $detailPoiModel->type_es = replace_string_based_on_dictionary($key, $poiNameMapping);
                    $detailPoiModel->description = $value;
                    $detailPoiModel->poi_id = $poi->id;

                    // Save place details to DB
                    Log::debug('Save DetailPOI to db', ['data' => $detailPoiModel]);
                    $detailPoiModel->save(); // Uncomment this to save records.
                }
            }

            $media_count = Media::where('poi_id', $poi->id)->count();
            // if don't have any media for this POI:
            if ($media_count == 0) {
                // If there is any media to parse:
                if (Arr::get($response, 'media_count') > 0 ) {
                    $mediaFromSygic = Arr::get($response, 'main_media');
                    $mediaFromSygic = $mediaFromSygic['media'];

                    foreach ($mediaFromSygic as $image) {

                        if (Arr::get($image, 'type') == 'photo') {
                            $mediaModel = new Media();
                            $mediaModel->url = Arr::get($image, 'url');
                            $mediaModel->poi_id = $poi->id;

                            Log::debug('Save Media to db', ['data' => $mediaModel]);
                            $mediaModel->save();
                        }
                    }

                }
            }

            // Update POI description.
            $description = Arr::get($response, 'description');
            $poi->description = $description['text'];
            Log::debug('Save Poi to db', ['data' => $poi]);
            $poi->save();
        }
    }
}
