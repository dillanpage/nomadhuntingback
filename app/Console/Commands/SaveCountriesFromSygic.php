<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Place;
use Exception;

class SaveCountriesFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connect to Sygic API, get the Countries, parse and save into out DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:country");

        $continents = [
            1 => 'europa',
            2 => 'africa',
            4 => 'asia',
            5 => 'oceania',
            6 => 'america-del-norte',
            7 => 'america-del-sur', 
        ];

        foreach ($continents as $continnentId => $continentName) {
            // Client without mock response
            $client = new Client();

            // Set headers
            $headers['x-api-key'] = env("SYGIC_KEY", "api-key");

            try {
                // Send request to Sygic
                $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/list?levels=country&parents=continent:'.$continnentId.'&limit=100', [
                    'headers' => $headers
                ]);

                $response = $request->getBody()->getContents();
                $places = json_decode($response, true);
                $places = collect($places['data']['places']);
            } catch (\Throwable $th) {
                throw new Exception("Error Processing Request");
            }
            
            $places->each(function ($place, $key) use ($continentName) {
                // Get the place details.
                $placeModel = new Place();
                
                $placeModel->sygic_id = Arr::get($place, 'id');
                $placeModel->level = Arr::get($place, 'level');
                $placeModel->name = Arr::get($place, 'name');
                
                // Check if country have another continent
                $url = format_uri(Arr::get($place, 'name'));
                $db_url = Arr::get(Place::where('url', $url)->first(), 'url');
                if ($url != $db_url) {
                    $placeModel->url = $url;
                } else {
                    $placeModel->url = $url . '_' . rand(1,100);
                }

                $placeModel->name_local = Arr::get($place, 'name_local');
                $placeModel->name_en = Arr::get($place, 'name_en');
                $placeModel->description = Arr::get($place, 'description.text');
                $placeModel->thumbnail_url = Arr::get($place, 'thumbnail_url');
                
                // Get parent continent id
                $placeModel->parent_continent = Place::where('url', $continentName)->first()->id;

                // Save place details to DB
                // $placeModel->save(); // Uncomment this to save records.
            });

        }
    }
}