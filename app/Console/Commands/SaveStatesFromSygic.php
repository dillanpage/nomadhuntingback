<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Place;
use Exception;

class SaveStatesFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:states';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:states");

        $countries = Place::where('level', 'country')->where('id', '<', 15)->get();

        foreach ($countries as $country) {
            // Client without mock response
            $client = new Client();

            // Set headers
            $headers['x-api-key'] = env("SYGIC_KEY", "api-key");

            try {
                // Send request to Sygic           
                $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/list?parents='.$country->sygic_id.'&levels=state&limit=300', [
                    'headers' => $headers
                ]);

                $response = $request->getBody()->getContents();
                $places = json_decode($response, true);
                $places = collect($places['data']['places']);
            } catch (\Throwable $th) {
                throw new Exception("Error Processing Request");
            }

            $places->each(function ($place, $key) use ($country) {
                // Get the place details.
                $placeModel = new Place();
                
                $placeModel->sygic_id = Arr::get($place, 'id');
                $placeModel->level = 'state';
                $placeModel->name = Arr::get($place, 'name');
                
                $url = format_uri(Arr::get($place, 'name'));
                $db_url = Arr::get(Place::where('url', $url)->first(), 'url');
                if ($url != $db_url) {
                    $placeModel->url = $url;
                } else {
                    $placeModel->url = $url . '_' . hexdec(uniqid());
                }

                $placeModel->name_local = Arr::get($place, 'name_local');
                $placeModel->name_en = Arr::get($place, 'name_en');
                $placeModel->description = Arr::get($place, 'description.text');
                $placeModel->thumbnail_url = Arr::get($place, 'thumbnail_url');
                
                // Get parent continent id
                $placeModel->parent_country = $country->id;
                $placeModel->parent_continent = $country->continent->id;

                // Save place details to DB
                Log::debug('Save state to db', ['data' => $placeModel]);
                // $placeModel->save(); // Uncomment this to save records.
            });

        }
    }
}
