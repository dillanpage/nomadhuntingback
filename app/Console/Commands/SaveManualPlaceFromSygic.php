<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Place;
use Exception;

class SaveManualPlaceFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:manual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:manual");

        $parent_country = 115;
        $parent_continent = 1;

        // Client without mock response
        $client = new Client();

        // Set headers
        $headers['x-api-key'] = env("SYGIC_KEY", "api-key"); 

        try {
            $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/city:315', [
                'headers' => $headers
            ]);

            $response = $request->getBody()->getContents();
            $place = json_decode($response, true);
            $place = collect($place['data']['place']);
        } catch (\Throwable $th) {
            throw new Exception("Error Processing Request");
        }

        // Get the place details.
        $placeModel = new Place();
        
        $placeModel->sygic_id = Arr::get($place, 'id');
        $placeModel->level = 'state';
        $placeModel->name = Arr::get($place, 'name');
        
        $url = format_uri(Arr::get($place, 'name'));
        $db_url = Arr::get(Place::where('url', $url)->first(), 'url');
        if ($url != $db_url) {
            $placeModel->url = $url;
        } else {
            $placeModel->url = $url . '_' . hexdec(uniqid());
        }

        $placeModel->name_local = Arr::get($place, 'name_local');
        $placeModel->name_en = Arr::get($place, 'name_en');
        $placeModel->description = Arr::get($place, 'description.text');
        $placeModel->thumbnail_url = Arr::get($place, 'thumbnail_url');

        $location = Arr::get($place, 'location');
        $placeModel->latitude = $location['lat'];
        $placeModel->longitude = $location['lng'];
        
        // Get parent continent id
        $placeModel->parent_country = $parent_country;
        $placeModel->parent_continent = $parent_continent;

        // Save place details to DB
        Log::debug('Save state to db', ['data' => $placeModel]);
        $placeModel->save(); // Uncomment this to save records.
    }
}
