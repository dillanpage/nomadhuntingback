<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Tag;
use Exception;

class SaveTagsFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:tags");

        $allowedTags = [
            'Art Centre',
            'Exhibition',
            'Exhibition Building',
            'Art Gallery',
            'Art Gallery',
            'National Gallery',
            'Botanical Garden',
            'Tropical Garden',
            'Museum',
            'Art Museum',
            'Open-air Museum',
            'Technology Museum',
            'Wax Museum',
            'Observatory',
            'Astronomical Observatory',
            'Planetarium',
            'Theatre',
            'University',
            'College',
            'Zoo',
            'Aquarium',
            'Sea World',
            'Aviary',
            'Petting Zoo',
            'Safari',
            'Wildlife',
            'Café',
            'Internet Cafe',
            'Tea House',
            'Food Court',
            'Restaurant',
            'Ethiopian Restaurant',
            'American Restaurant',
            'American Diner',
            'Asian Restaurant',
            'Arabic Restaurant',
            'Chinese Restaurant',
            'Indonesian Restaurant',
            'Japanese Restaurant',
            'Korean Restaurant',
            'Lebanese Restaurant',
            'Noodles',
            'Ramen',
            'Sushi',
            'Thai Restaurant',
            'Vietnamese Restaurant',
            'Austrian Restaurant',
            'Belgian Restaurant',
            'Bistro',
            'Brasserie',
            'British Restaurant',
            'Burgers',
            'Creperie',
            'Curry',
            'Czech Restaurant',
            'Dutch Restaurant',
            'Fish',
            'Fish & Chips',
            'French Restaurant',
            'Friterie',
            'Georgian Restaurant',
            'German Restaurant',
            'Bavarian Restaurant',
            'Greek Restaurant',
            'Indian Restaurant',
            'International Restaurant',
            'Italian Restaurant',
            'Local Restaurant',
            'Mediterranean Restaurant',
            'Mexican Restaurant',
            'Burritos',
            'Oriental Restaurant',
            'Pasta',
            'Pizza',
            'Portuguese Restaurant',
            'Regional Restaurant',
            'Russian Restaurant',
            'Seafood Restaurant',
            'Spanish Restaurant',
            'Tapas Restaurant',
            'Steakhouse',
            'Traditional Restaurant',
            'Turkish Restaurant',
            'Vegan Restaurant',
            'Vegetarian Restaurant',
            'Donuts',
            'Pancakes',
            'Creperie',
            'Bar',
            'Cocktail Bar',
            'Ice Bar',
            'Piano Bar',
            'Cinema',
            'Circus',
            'Comedy Club',
            'Music Club',
            'Nightclub',
            'Brothel',
            'Community Centre',
            'Concert Hall',
            'Cultural Center',
            'Opera',
            'Pub',
            'Beer Garden',
            'Brewpub',
            'Event Venue',
            'Wine Bar',
            'Wine Cellar',
            'Winery',
            'Alpine Hut',
            'Barbecue',
            'Cottage',
            'Hut',
            'Wilderness Hut',
            'Cave',
            'Ice Cave',
            'Lava Cave',
            'Cliff',
            'Forest',
            'Glacier',
            'Grill',
            'Bird Hide',
            'Wildlife Hide',
            'Hill',
            'Tumulus',
            'Hot Springs',
            'Lake',
            'Meadow',
            'Megalith',
            'Natural Monument',
            'Mountains',
            'Mountain Pass',
            'Peak',
            'City Park',
            'National Park',
            'Nature Park',
            'Nature Reserve',
            'Path',
            'Via Ferrata',
            'Picnic Site',
            'Firepit',
            'Picnic Shelter',
            'Picnic Table',
            'Rapids',
            'Rock',
            'Cairn',
            'Stone',
            'Saddle',
            'Weather Shelter',
            'Sinkhole',
            'Stream',
            'Tree',
            'Valley',
            'View Point',
            'Bird Watching',
            'Vineyard',
            'Volcano',
            'Waterfall',
            'Wetland',
            'Bog',
            'Marsh',
            'Swamp',
            'Wood',
            'Arcade',
            'Suspension Bridge',
            'Viaduct',
            'Bunker',
            'Convention Centre',
            'Embassy',
            'Public Building',
            'Bell Tower',
            'Life Guard Tower',
            'Water Tower',
            'Tunnel',
            'Cemetery',
            'Christian Cemetery',
            'Crematorium',
            'Grave',
            'War Grave',
            'Jewish Cemetery',
            'Mortuary',
            'Muslim Cemetery',
            'Dog Park',
            'Drinking Water',
            'Tourist Information',
            'Information Board',
            'Map',
            'Guidepost',
            'Tourist Information Office',
            'Visitor Centre',
            'Tourist Information Terminal',
            'Coworking Space',
            'Governmental Office',
            'Place of Worship',
            'Chapel',
            'Church',
            'Basilica',
            'Cathedral',
            'Monastery',
            'Mosque',
            'Parish Hall',
            'Shrine',
            'Synagogue',
            'Temple',
            'Greek Temple',
            'Theme Park',
            'Disney Park',
            'Amusement Ride',
            'Roller Coaster',
            'Universal',
            'Water Park',
            'Water Slide',
            'Beach',
            'Gravel Beach',
            'Nude Beach',
            'Beach Resort',
            'Sandy Beach',
            'Park',
            'Garden',
            'Bakery',
            'Beauty Salon',
            'Alcohol Shop',
            'Tea Shop',
            'Wine Shop',
            'Bicycle Shop',
            'Book Maker',
            'Bookshop',
            'Book Binder',
            'Butcher',
            'Shopping Centre',
            'Shopping District',
            'Shopping Mall',
            'Lottery',
            'Market',
            'Farm Produce',
            'Fish Market',
            'Flea Market',
            'Food Market',
            'Street Market',
            'Attraction',
            'Aircraft',
            'Aquaduct',
            'Archaeological Site',
            'Architecture',
            'Modern Architecture',
            'Artwork',
            'Mural',
            'Public Art',
            'Battlefield',
            'Brewery',
            'Bridge',
            'Building',
            'Court',
            'Burrial Site',
            'Castle',
            'Castle Ruins',
            'Chateau',
            'City Wall',
            'City Gate',
            'Distillery',
            'Ferris Wheel',
            'Fort',
            'Blockhouse',
            'Fortification',
            'Fountain',
            'Heritage Site',
            'Historic Building',
            'Landmark',
            'Library',
            'Lighthouse',
            'Marina',
            'Maze',
            'Memorial',
            'War Memorial',
            'Cannon',
            'Mill',
            'Water Mill',
            'windmill',
            'Monument',
            'National Monument',
            'Obelisk',
            'Observation Deck',
            'Palace',
            'Parliament',
            'Ruins',
            'Sculpture',
            'Ship',
            'Shipwreck',
            'Skyscraper',
            'Statue',
            'Tomb',
            'Mausoleum',
            'Tower',
            'Campanile',
            'Lookout Tower',
            'Observation Tower',
            'Town Hall',
            'City Hall'
        ];


        // Do a request to get all tags from Sygic
        $client = new Client();

        // Set headers
        $headers['x-api-key'] = env("SYGIC_KEY", "api-key");
        
        try {
            // Send request to Sygic           
            $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/stats', [
                'headers' => $headers
            ]);

            $response = $request->getBody()->getContents();
            $tags = json_decode($response, true);
            $tags = collect($tags['data']['stats']['tags']);
        } catch (\Throwable $th) {
            throw new Exception("Error Processing Request");
        }

        $tags->each(function ($tag, $key) use ($allowedTags) {
            // Get the place details.
            $tagModel = new Tag();
            
            $tag_name = Arr::get($tag, 'key', '');
            if (in_array($tag_name, $allowedTags)) {
                $tagModel->name_en = Arr::get($tag, 'key');
                $tagModel->name = Arr::get($tag, 'name');
                $tagModel->count = Arr::get($tag, 'count');

                $url = format_uri(Arr::get($tag, 'name'));
                $db_url = Arr::get(tag::where('url', $url)->first(), 'url');
                if ($url != $db_url) {
                    $tagModel->url = $url;
                } else {
                    $tagModel->url = $url . '_' . hexdec(uniqid());
                }

                // Save tag details to DB
                Log::debug('Save state to db', ['data' => $tagModel]);
                // $tagModel->save(); // Uncomment this to save records.
            }
        });




        
    }
}
