<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Place;
use App\Poi;
use App\Tag;
use App\DetailPoi;
use App\Category;
use App\TagsNotAllowed;
use Exception;

class SavePoisFromSygic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getDataFromSygic:pois';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug("Starting command getDataFromSygic:pois");

        $cities = Place::where('id', 6062)->get();
        $tags = Tag::all();
        $tagsNotAllowed = TagsNotAllowed::all();
        $tagsNotAllowed = $tagsNotAllowed->pluck('name')->toArray();



        foreach ($cities as $city) {
            // Client without mock response
            $client = new Client();

            // Set headers
            $headers['x-api-key'] = env("SYGIC_KEY", "api-key");

            try {
                // Send request to Sygic           
                $request = $client->request('GET', 'https://api.sygictravelapi.com/1.2/es/places/list?parents='.$city->sygic_id.'&levels=poi&limit=100', [
                    'headers' => $headers
                ]);

                $response = $request->getBody()->getContents();
                $pois = json_decode($response, true);
                $pois = collect($pois['data']['places']);
            } catch (\Throwable $th) {
                throw new Exception("Error Processing Request");
            }

            $pois->each(function ($poi, $key) use ($city, $tags, $tagsNotAllowed) {
                
                $sygicPoiTags = Arr::get($poi, 'tag_keys', []);
                $allowedPoi = true;

                // Check if any tag from sygic tags it not allowed, then dont add this POI.
                foreach ($sygicPoiTags as $sygicTag) {
                    if(in_array($sygicTag, $tagsNotAllowed)) {
                        $allowedPoi = false;
                    }
                }

                if ($allowedPoi) {
                    $poiModel = new Poi();
                
                    $poiModel->sygic_id = Arr::get($poi, 'id');
                    $poiModel->name = Arr::get($poi, 'name');

                    $url = format_uri(Arr::get($poi, 'name'));
                    $db_url = Arr::get(Poi::where('url', $url)->first(), 'url');
                    if ($url != $db_url) {
                        $poiModel->url = $url;
                    } else {
                        $poiModel->url = $url . '_' . hexdec(uniqid());
                    }

                    $poiModel->name_local = Arr::get($poi, 'name_local');
                    $poiModel->name_en = Arr::get($poi, 'name_en');

                    $location = Arr::get($poi, 'location');
                    $poiModel->latitude = $location['lat'];
                    $poiModel->longitude = $location['lng'];
                    $poiModel->rating = Arr::get($poi, 'rating');
                    $poiModel->duration_estimate = Arr::get($poi, 'duration_estimate');
                    $poiModel->description = Arr::get($poi, 'perex');
                    $poiModel->thumbnail_url = Arr::get($poi, 'thumbnail_url');
                    
                    // place_id
                    $poiModel->place_id = $city->id;

                    // Save place details to DB
                    Log::debug('Save POI to db', ['data' => $poiModel]);
                    $poiModel->save(); // Uncomment this to save records.

                    // Save poi tags.
                    foreach ($sygicPoiTags as $sygicTag) {
                        // find tag.
                        $tag = Tag::where('name_en', $sygicTag)->first();

                        // If tag exists
                        if ($tag) {
                            $poiModel->tags()->attach($tag);
                        } else {
                            $newTag = new Tag();
                            $newTag->name_en = $sygicTag;
                            $newTag->url = format_uri($sygicTag);
                            $newTag->save();

                            // Attach tag to Poi
                            $poiModel->tags()->attach($newTag);
                        }
                        
                    }

                    // Save poi categories.
                    $sygicCategories = Arr::get($poi, 'categories', []);
                    foreach ($sygicCategories as $sygicCategory) {
                        // find tag.
                        $category = Category::where('name_en', $sygicCategory)->first();
                        $poiModel->categories()->attach($category);
                    }
                }
            });

        }

    }
}
