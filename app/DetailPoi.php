<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPoi extends Model
{
    protected $fillable = ['type', 'description'];

    public function poi()
    {
        return $this->belongsTo(Poi::class);
    }
}
