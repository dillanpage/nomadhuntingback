<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagsNotAllowed extends Model
{
    protected $table = "tags_not_allowed";
    protected $fillable = ['name'];
}