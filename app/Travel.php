<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['start_date', 'end_date'];

    // Get the place where this Travel belongs
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    // Get the User this Travel belongs
    public function user()
    {
        return $this->belongsTo(Place::class);
    }

    // Pivot table is user_poi
    public function pois()
    {
        return $this->belongsToMany(Poi::class);
    }
}
