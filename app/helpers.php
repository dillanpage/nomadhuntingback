<?php
/**
* Reemplaza todos los acentos por sus equivalentes sin ellos
*
* @param $string
*  string la cadena a sanear
*
* @return $string
*  string saneada
*/


if (! function_exists('format_uri')) {
    function format_uri($string)
    {

        $string = trim($string);
        $string = strtolower($string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\"", "¨", "º", "~",
                    "#", "@", "|", "!", "'",
                    "·", "$", "%", "&", "/",
                    "(", ")", "?", "'", "¡",
                    "¿", "[", "^", "<code>", "]",
                    "+", "}", "{", "¨", "´",
                    ">", "< ", ";", ",", ":",
                    "."),
            '',
            $string
        );

        $string = str_replace(
                array(' '),
                array('-'),
            $string
            );


        $string = str_replace(
            array('--'),
            array('-'),
            $string
            );

        $string = strtolower($string);

        return $string;
    }
}

if (! function_exists('replace_string_based_on_dictionary')) {
    function replace_string_based_on_dictionary($toBeReplaced, $collection)
    {
        $replacedValue = '-';
        $collection->each( function($newValue, $replaceKey) use (&$toBeReplaced, &$replacedValue) {
            if ($replaceKey == $toBeReplaced) {
                $replacedValue = $newValue;
            }
        });

        return $replacedValue;
    }
}


?>