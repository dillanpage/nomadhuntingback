<?php

namespace App\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Handler\MockHandler;

class Client extends GuzzleClient
{
    private $client;

    /**
     * Constructor
     * 
     * @params
     * Mock
     * timeout
     */
    public function __construct($body = null, $time_out = 10)
    {
        $config = ['timeout' => $time_out];

        if ($body) {
            Log::info("Guzzle client is a mock version");

            // Get the mock
            //$body = "mock response";

            // Set the mock to the client
            $mock = new MockHandler([
                new Response(200, [], $body)
            ]);

            // Create a client with the mock
            $handler = HandlerStack::create($mock);
            $config['handler'] = $handler;
        }

        // Construct the client with GuzzleClient class.
        parent::__construct($config);
    }
}