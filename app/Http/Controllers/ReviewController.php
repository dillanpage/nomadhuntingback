<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Media;

class ReviewController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function userReviews(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $reviews = Review::where('user_id', $user->id)->with(['poi', 'place', 'media'])->get();
            return $reviews->toJson();
        }

        return null;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            // Create new review
            $review = new Review();

            $review->description = $request->description;
            $review->rating = (int) $request->rating;
            $review->user_id = $user->id;
            $review->poi_id = $request->poi_id;
            $review->place_id = $request->place_id;
            $review->save();

            if($request->hasFile('image')){  
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult();    
                
                $media = new Media();
                $media->url = $cloudinaryResponse['url'];
                $media->review_id = $review->id;
                $media->user_id = $user->id;
                $media->place_id = $request->place_id;
                $media->poi_id = $request->poi_id;
                $media->save();
            }

            $response = ["error" => false, "data" => "Review added successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * Delete User review
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteReview(Request $request, $reviewId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $review = Review::where('user_id', $user->id)->where('id', $reviewId)->first();
            $mediaToDelete = $review->media();

            $mediaToDelete->each(function($item, $key) {
                // Print each city name
                $imageUrl = $item->url;
                $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                $explodedImage = explode("/", $imageUrl);
                $imageUrl = $explodedImage[1];

                $explodedImage = explode(".", $imageUrl);
                $imageUrl = $explodedImage[0];

                // Delete image from Cloudinary
                \Cloudder::delete($imageUrl);
                $item->delete();
            });
            
            $review->delete();
            
            $response = ["error" => false, "data" => "Review deleted"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function getPendingReviews(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user->isAdmin()) {
            $reviews = Review::where('status', 'pending')->with(['poi', 'place', 'media'])->get();
            return $reviews->toJson();
        }

        return null;
    }

    /**
    * Approve pending review
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function approveReview(Request $request, $reviewId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user->isAdmin()) {
            $review = Review::where('status', 'pending')->where('id', $reviewId)->first();
            $review->status = 'approved';
            $review->save();

            $response = ["error" => false, "data" => "Review approved"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
    * Deny pending review
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function denyReview(Request $request, $reviewId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user->isAdmin()) {
            $review = Review::where('status', 'pending')->where('id', $reviewId)->first();
            $review->status = 'denied';
            $review->save();

            $mediaToDelete = $review->media();

            $mediaToDelete->each(function($item, $key) {
                $imageUrl = $item->url;
                $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                $explodedImage = explode("/", $imageUrl);
                $imageUrl = $explodedImage[1];

                $explodedImage = explode(".", $imageUrl);
                $imageUrl = $explodedImage[0];

                // Delete image from Cloudinary
                \Cloudder::delete($imageUrl);
                $item->delete();
            });

            $response = ["error" => false, "data" => "Review denied"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }
}
