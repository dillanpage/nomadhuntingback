<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suggestion;

class SuggestionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function index(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $suggestions = Suggestion::where('user_id', $user->id)->orderby('created_at', 'desc')->get();
            
            $response = ["error" => false, "data" => $suggestions];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            // Create new Suggestion
            $suggestion = new Suggestion();

            $suggestion->title = $request->title;
            $suggestion->description = $request->description;
            $suggestion->type = $request->type;
            $suggestion->status = 'Pending';
            $suggestion->user_id = $user->id;
            $suggestion->save();

            $response = ["error" => false, "data" => "Suggestion added successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
    * Deny pending suggestion
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function deny(Request $request, $suggestionId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user->isAdmin()) {
            $suggestion = Suggestion::where('id', $suggestionId)->first();
            $suggestion->status = 'Denied';
            $suggestion->response = $request->response;
            $suggestion->save();

            $response = ["error" => false, "data" => "Suggestion denied"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
    * Accept pending suggestion
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function accept(Request $request, $suggestionId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user->isAdmin()) {
            $suggestion = Suggestion::where('id', $suggestionId)->first();
            $suggestion->status = 'Accepted';
            $suggestion->response = $request->response;
            $suggestion->save();

            $response = ["error" => false, "data" => "Suggestion accepted"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }
}
