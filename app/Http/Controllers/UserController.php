<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Poi;
use App\Travel;
use App\Place;

class UserController extends Controller
{
    /**
     * Store a poi like given from a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userLikesPoi(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            // Create new poi
            $poi = Poi::where('id', $request->id)->first();

            // Save relationship data if not exists
            if (!$user->pois()->where('poi_id', $poi['id'])->exists()) {
                $user->pois()->attach($poi);
            }

            $response = ["error" => false, "data" => "User likes POI successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * User stop liking a Poi :(
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userUnlikesPoi(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            // Create new poi
            $poi = Poi::where('id', $request->id)->first();

            // Save relationship data if not exists
            if ($user->pois()->where('poi_id', $poi['id'])->exists()) {
                $user->pois()->detach($poi);
            }

            $response = ["error" => false, "data" => "User does not like the interest place :("];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * Retrieve liked pois for a user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function retrieveLikedPois(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            // Fetch user liked pois
            $pois = $user->pois()->with(['place', 'categories'])->get();

            $places = collect();

            $pois->each(function ($poi, $key) use (&$places){
                !$places->contains($poi->place) ? $places->push($poi->place) : '';
            });

            // Add pois to the place belongs.
            $places->each(function ($place, $key) use ($pois){
                $filteredPois = $pois->where('place.id', '=', $place->id);
                $place->pois = $filteredPois->toArray();
            });

            $response = ["error" => false, "data" => $places];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }

        return null;
    }

    /**
     * User adds a travel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addTravel(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        $start_date = Carbon::createFromFormat('d/m/Y', $request->dateFrom)->format('Y-m-d');
        $end_date = Carbon::createFromFormat('d/m/Y', $request->dateTo)->format('Y-m-d');

        $request->validate([
            'place'     => 'required|integer',
            'pois'      => 'nullable|array',
            'pois.*'    => 'integer'
        ]);

        if ($user) {
            $travel = new Travel([
                'start_date' => $start_date,
                'end_date'   => $end_date
            ]);

            $travel->user_id = $user->id;
            $travel->place_id = $request->place;

            $travel->save();

            if (count($request->pois) > 0) {
                $travel->pois()->attach($request->pois);
            }
            
            $response = ["error" => false, "data" => 'Travel added'];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Get User travels
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTravels(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $travels = Travel::where('user_id', $user->id)->with(['pois'])->with(['place' => function($query) {
                $query->with('country');
            }])->orderBy('end_date', 'desc')->get();
            
            $response = ["error" => false, "data" => $travels];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Get User travel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTravel(Request $request, $travelId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $travel = Travel::where('user_id', $user->id)->where('id', $travelId)->with(['place', 'pois'])->first();
            
            $response = ["error" => false, "data" => $travel];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Get User travel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteTravel(Request $request, $travelId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $travel = Travel::where('user_id', $user->id)->where('id', $travelId)->first();
            $travel->pois()->sync([]);
            $travel->delete();
            
            $response = ["error" => false, "data" => "Travel deleted"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Edit User travel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editTravel(Request $request, $travelId)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $travel = Travel::where('user_id', $user->id)->where('id', $travelId)->first();
            
            $travel->pois()->sync($request->pois);

            $response = ["error" => false, "data" => "Travel POIs updated"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Get User travel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function visitedPois(Request $request)
    {
        // Validate user exists
        $user = $request->user();
        // $visitedPois = [];
        $today = Carbon::now();

        if ($user) {
            $travels = Travel::where('user_id', $user->id)->where('end_date', '<', $today->format('Y-m-d'))->with(['pois', 'place'])->get();
            
            $response = ["error" => false, "data" => $travels];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Get User private profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPrivateProfile(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {
            $userProfile = User::where('id', $user->id)->with('place')->first();
            
            $response = ["error" => false, "data" => $userProfile];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Edit User private profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editUserProfile(Request $request)
    {
        // Validate user exists
        $user = $request->user();

        if ($user) {

            $updatedProfile = json_decode($request->user);
            $user->name = $updatedProfile->name;
            $user->lastname = $updatedProfile->lastname;
            $user->username = $updatedProfile->username;
            $user->birthday = $updatedProfile->birthday;
            $user->description = $updatedProfile->description;
            $user->place_id = $updatedProfile->place_id;
            $user->is_private = $updatedProfile->is_private;

            if($request->hasFile('image')){
                if (strpos($request->original_photo_url, 'cloudinary.com')) {
                    
                    $imageUrl = $request->original_photo_url;
                    $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                    $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                    $explodedImage = explode("/", $imageUrl);
                    $imageUrl = $explodedImage[1];

                    $explodedImage = explode(".", $imageUrl);
                    $imageUrl = $explodedImage[0];

                    // Delete image from Cloudinary
                    \Cloudder::delete($imageUrl);
                }
                
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult(); 
                
                $user->photo_url = $cloudinaryResponse['url'];
            }
            $user->save();
            
            $response = ["error" => false, "data" => "User updated successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    public function getUserByUsername(Request $request, $username) {
        $user = User::where('username', $username)->where('is_private', 0)->with(['place', 'travels' => function($query) {
            $query->with(['place', 'pois']);
        }])->first();

        if($user) {
            $response = ["error" => false, "data" => $user];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        } else {
            $response = ["error" => true, "data" => "User not found"];
            return response(json_encode($response), 404, ["Content-Type" => "application/json"]);
        }
    }

    public function getUsersByPlace(Request $request, $place) {
        $user = $request->user();

        if($user) {
            // Places
            $places = Place::where('name', 'like', '%'.$place.'%')->limit(10)->get()->pluck('id');

            $users = User::whereIn('place_id', $places)->with(['place'])->where('is_private', 0)->limit(20)->get();

            $response = ["error" => false, "data" => $users];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    public function getUsersByUsername(Request $request, $username) {
        $user = $request->user();
        
        if($user) {
            $users = User::where('username', 'like', '%'.$username.'%')->with(['place'])->where('is_private', 0)->limit(20)->get();

            $response = ["error" => false, "data" => $users];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }
}
