<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poi;
use App\Category;
use App\Review;
use App\DetailPoi;
use App\Media;
use Illuminate\Support\Facades\DB;

/**
* @OA\Info(title="RESTApi POIs", version="1.0")
*
* @OA\Server(url="http://local.api-nomadhunting.com/public")
*/
class PoiController extends Controller
{

    /**
    * @OA\Get(
    *     path="/api/pois",
    *     summary="Show pois",
    *     @OA\Response(
    *         response=200,
    *         description="Show all the available pois."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="An error occured."
    *     )
    * )
    *
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function index()
    {
        $pois = Poi::orderby('rating', 'desc')->with(['categories', 'tags'])->withCount(['users', 'reviews'])->limit(50)->get();
        return $pois->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create new poi
        $poi = new Poi();

        // Add data
        $poi->name = $request->name;

        // Save changes in our model
        $poi->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($url)
    {
        // Request a resource based on id
        return Poi::where('url', $url)->with(['categories', 'details_poi', 'tags', 'place', 'media'])
        ->with(['reviews' => function($query){
            $query->where('status', '=', 'approved')->with('user');
        }])->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $poiId)
    {
        // Validate user exists and is admin
        $user = $request->user()->isAdmin();
        $requestPoi = json_decode($request->poi);
        $requestCategories = $requestPoi->cat;
        $requestDetails = $requestPoi->details;

        if ($user) {
            $poi = Poi::where('id', $poiId)->first();

            $poi->name = $requestPoi->name;
            $poi->url = $requestPoi->url;
            $poi->latitude = $requestPoi->latitude;
            $poi->longitude = $requestPoi->longitude;
            $poi->description = $requestPoi->description;
            $poi->place_id = $requestPoi->place_id;

            if($request->hasFile('image')){
                if (strpos($request->original_photo_url, 'cloudinary.com')) {
                    
                    $imageUrl = $request->original_photo_url;
                    $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                    $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                    $explodedImage = explode("/", $imageUrl);
                    $imageUrl = $explodedImage[1];

                    $explodedImage = explode(".", $imageUrl);
                    $imageUrl = $explodedImage[0];

                    // Delete image from Cloudinary
                    \Cloudder::delete($imageUrl);
                }
                
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult();
                
                $poi->thumbnail_url = $cloudinaryResponse['url'];
            }

            $poi->save();

            // Update categories
            $poi->categories()->sync([]);
            foreach ($requestCategories as $key => $value) {
                $poi->categories()->attach($value);
            }

            // Update details
            // Delete Details
            $detailPois = DetailPoi::where('poi_id', $poiId)->get();
            $detailPois->each(function ($item, $key) {
                $item->delete();
            });

            // Insert details
            if ($requestDetails->opening_hours) {
                $detail = new DetailPoi();
                $detail->type='opening_hours';
                $detail->type_es='Horario';
                $detail->description = $requestDetails->opening_hours;
                $detail->poi_id = $poiId;
                $detail->save();
            }

            if ($requestDetails->address) {
                $detail = new DetailPoi();
                $detail->type='address';
                $detail->type_es='Dirección';
                $detail->description = $requestDetails->address;
                $detail->poi_id = $poiId;
                $detail->save();
            }

            if ($requestDetails->admission) {
                $detail = new DetailPoi();
                $detail->type='admission';
                $detail->type_es='Admisión';
                $detail->description = $requestDetails->admission;
                $detail->poi_id = $poiId;
                $detail->save();
            }

            if ($requestDetails->phone) {
                $detail = new DetailPoi();
                $detail->type='phone';
                $detail->type_es='Teléfono';
                $detail->description = $requestDetails->phone;
                $detail->poi_id = $poiId;
                $detail->save();
            }

            if ($requestDetails->email) {
                $detail = new DetailPoi();
                $detail->type='email';
                $detail->type_es='Email';
                $detail->description = $requestDetails->email;
                $detail->poi_id = $poiId;
                $detail->save();
            }

            $response = ["error" => false, "data" => "Poi updated"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $poi = Poi::where('id', $id)->first();

        // delete all dependencies.
        $poi->categories()->sync([]);
        $poi->tags()->sync([]);
        $poi->travels()->sync([]);
        $poi->users()->sync([]);

        // Delete Details
        $detailPois = DetailPoi::where('poi_id', $id)->get();
        $detailPois->each(function ($item, $key) {
            $item->delete();
        });

        // Delete Reviews
        $reviewPois = Review::where('poi_id', $id)->get();
        $reviewPois->each(function ($item, $key) {
            $item->delete();
        });

        // Remove all media related
        $mediaPois = Media::where('poi_id', $id);
        $mediaPois->each(function ($item, $key) {
            if (strpos($item->url, 'cloudinary.com')) {
                $imageUrl = $item->url;
                $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                $explodedImage = explode("/", $imageUrl);
                $imageUrl = $explodedImage[1];

                $explodedImage = explode(".", $imageUrl);
                $imageUrl = $explodedImage[0];

                // Delete image from Cloudinary
                \Cloudder::delete($imageUrl);

                $item->delete();
            }
        });

        $poi->delete();
        
        $response = ["error" => false, "data" => "Poi deleted"];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    /**
     * Display top liked pois
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usersLikedPois()
    {

        $likedPois = DB::select('SELECT count(poi_id) as total, p.*
            FROM poi_user pu
            LEFT JOIN pois p on (p.id = pu.poi_id)
            GROUP BY poi_id
            ORDER BY total desc
            LIMIT 25;');

        $response = ["error" => false, "data" => $likedPois];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    /**
    * Look for POIs that matches the requested name
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function searchPois($search)
    {
        $pois = Poi::where('name', 'like', '%'.$search.'%')->orderby('rating', 'desc')->with(['categories', 'tags'])->withCount(['users', 'reviews'])->limit(25)->get();

        $response = ["error" => false, "data" => $pois];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    public function getDetailTypes() 
    {
        return [    ["type" => "opening_hours", "name" => "Horario"],
                    ["type" => "address", "name" => "Dirección"],
                    ["type" => "admission", "name" => "Admisión"],
                    ["type" => "email", "name" => "Email"],
                    ["type" => "phone", "name" => "Teléfono"]
                ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminStore(Request $request)
    {
        // Validate user exists
        $user = $request->user();
        $poi = new Poi();

        if ($user) {

            $requestPoi = json_decode($request->poi);
            $poi->name = $requestPoi->name;
            $poi->url = $requestPoi->url;
            $poi->description = $requestPoi->description ?? null;
            $poi->latitude = $requestPoi->latitude ?? null;
            $poi->longitude = $requestPoi->longitude ?? null;
            $poi->place_id = $requestPoi->place_id ?? null;

            if($request->hasFile('image')){
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult(); 
                
                $poi->thumbnail_url = $cloudinaryResponse['url'];
            }

            $poi->save();

            // Sync categories
            $poi->categories()->sync($requestPoi->categories);

            // Save details
            foreach ($requestPoi->details as $key => $value) {
                $detailPoi = new DetailPoi();
                $detailPoi->type = $key;
                $detailPoi->description = $value;
                $detailPoi->poi_id = $poi->id;
                $detailPoi->save();
            }
            
            $response = ["error" => false, "data" => "Poi created successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Admin get poi
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPoi($id)
    {
        $poi = Poi::where('id', $id)->with(['place', 'details_poi'])->first();
        $categories = $poi->categories->pluck('id')->toArray();
        $poi['cat'] = $categories;

        $response = ["error" => false, "data" => $poi];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    /**
     * Admin get poi
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPoiCategories($id)
    {
        $poi = Poi::where('id', $id)->first();

        $categories = Category::get();

        $poiCategories = $poi->categories;

        $categories->each(function ($item, $key) use ($poiCategories) {
            $item->checked = false;
            foreach ($poiCategories as $category => $value) {
                if ($item->id == $value->id) {
                    $item->checked = true;
                }
            }
        });
        
        $response = ["error" => false, "data" => $categories];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }
}
