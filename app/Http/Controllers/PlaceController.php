<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Poi;

/**
* @OA\Info(title="RESTApi Places", version="1.0")
*
* @OA\Server(url="http://local.api-nomadhunting.com/public")
*/
class PlaceController extends Controller
{

    /**
    * @OA\Get(
    *     path="/api/places",
    *     summary="Show places",
    *     @OA\Response(
    *         response=200,
    *         description="Show all the available places."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="An error occured."
    *     )
    * )
    *
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    *
    */
    public function index()
    {
        $places = Place::where('level', 'country')->orderby('name', 'asc')->with(['state', 'city'])->limit(100)->get();
        return $places->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $places = Place::where('name', 'like', '%'.$name.'%')
                    ->where('level', '!=', 'continent')
                    ->orderByRaw( "FIELD(`level`, 'country', 'state', 'city')" )
                    ->with(['country'])
                    ->limit(10)
                    ->get();
        return $places->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $placeId)
    {
        // Validate user exists and is admin
        $user = $request->user()->isAdmin();
        $requestPlace = json_decode($request->place);

        if ($user) {

            $place = Place::where('id', $placeId)->first();

            $place->name = $requestPlace->name;
            $place->level = $requestPlace->level;
            $place->url = $requestPlace->url;
            $place->latitude = $requestPlace->latitude;
            $place->longitude = $requestPlace->longitude;
            $place->description = $requestPlace->description;
            $place->parent_country = $requestPlace->parent_country;
            $place->parent_state = $requestPlace->parent_state;

            if($request->hasFile('image')){
                if (strpos($request->original_photo_url, 'cloudinary.com')) {
                    
                    $imageUrl = $request->original_photo_url;
                    $stringToRemove = 'http://res.cloudinary.com/toniramon/image/upload/';
                    $imageUrl = str_replace($stringToRemove, "", $imageUrl);

                    $explodedImage = explode("/", $imageUrl);
                    $imageUrl = $explodedImage[1];

                    $explodedImage = explode(".", $imageUrl);
                    $imageUrl = $explodedImage[0];

                    // Delete image from Cloudinary
                    \Cloudder::delete($imageUrl);
                }
                
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult(); 
                
                $place->thumbnail_url = $cloudinaryResponse['url'];
            }
        
            $place->save();

            $response = ["error" => false, "data" => "Place updated"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $place = Place::where('id', $id)->first();
        $place->delete();
        
        $response = ["error" => false, "data" => "Place deleted"];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function placeWithPois($url)
    {
        $place = Place::where('url', $url)->first();

        if ($place->level == 'city') {
            $places = Place::where('url', $url)
                ->with(['country', 'state'])
                ->with(['pois' => function($query){
                    $query->orderBy('rating', 'desc')->with('categories')->withCount(['users', 'reviews']);
                }])->first();
        } else if ($place->level == 'state') {
            // $citiesIds = Place::where('parent_state', $place->id)->pluck('id')->toArray();
            $pois = Poi::where('place_id', $place->id)->with('categories')->withCount(['users', 'reviews'])->limit(100)->get();
            
            $places = Place::where('url', $url)
                ->with('country')
                ->first();
            
            $places['pois'] = $pois;
        } else if ($place->level == 'country') {
            $citiesIds = Place::where('parent_country', $place->id)->pluck('id')->toArray();
            $pois = Poi::whereIn('place_id', $citiesIds)->with('categories')->withCount(['users', 'reviews'])->limit(100)->get();
            
            $places = Place::where('url', $url)
                ->with('country')
                ->first();
            
            $places['pois'] = $pois;
        } else {
            return null;
        }
        
        return $places->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function placeWithCategories($url)
    {
        $categories = array();

        $place = Place::where('url', $url)->first();
        // Get all the POIs of a place.
        if ($place->level == 'city') {
            $places = Place::where('url', $url)
                ->with(['country', 'state'])
                ->with(['pois' => function($query){
                    $query->orderBy('rating', 'desc')->with('categories');
                }])->first();
        } else if ($place->level == 'state') {
            // $citiesIds = Place::where('parent_state', $place->id)->pluck('id')->toArray();
            $pois = Poi::where('place_id', $place->id)->with('categories')->limit(100)->get();
            
            $places = Place::where('url', $url)
                ->with('country')
                ->first();
            
            $places['pois'] = $pois;
        } else if ($place->level == 'country') {
            $citiesIds = Place::where('parent_country', $place->id)->pluck('id')->toArray();
            $pois = Poi::whereIn('place_id', $citiesIds)->with('categories')->limit(100)->get();
            
            $places = Place::where('url', $url)
                ->with('country')
                ->first();
            
            $places['pois'] = $pois;
        } else {
            return null;
        }

        // Get the categories of all these POIs
        $placePois = $places->pois;
        $placePois->each(function ($poi, $key) use (&$categories) {
            $poiCategories = $poi['categories'];
            foreach ($poiCategories as $category) {
                $categories[$category['url']] = $category->toArray();
            }
        });

        return $categories;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function searchPoisFromCity(Request $request, $city)
    {
        $pois = Place::where('url', $city)->first()->filteredPoisContainingName($request->filter)->get();
        
        $response = ["error" => false, "data" => $pois];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    public function findCities($cities)
    {
        $places = Place::where('name', 'like', '%'.$cities.'%')
                    ->where('level', 'city')
                    ->with(['country'])
                    ->limit(10)
                    ->get();

        $response = ["error" => false, "data" => $places];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);

    } 
    
    public function findStates($states)
    {
        $places = Place::where('name', 'like', '%'.$states.'%')
                    ->where('level', 'state')
                    ->with(['country'])
                    ->limit(10)
                    ->get();

        $response = ["error" => false, "data" => $places];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);

    } 

    public function findCountries($countries)
    {
        $places = Place::where('name', 'like', '%'.$countries.'%')
                    ->where('level', 'country')
                    ->limit(10)
                    ->get();

        $response = ["error" => false, "data" => $places];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);

    } 

    public function findAll($search)
    {
        $places = Place::where('name', 'like', '%'.$search.'%')
                    ->with('country')
                    ->OrderBy('name')
                    ->limit(25)
                    ->get();

        $response = ["error" => false, "data" => $places];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);

    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function placesMostLiked()
    {
        $places = Place::whereIn('id', [6051,6052,6053,6055,6057,6058,6059,6060,6061,6062])->get();
        
        $response = ["error" => false, "data" => $places];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminStore(Request $request)
    {
        // Validate user exists
        $user = $request->user();
        $place = new Place();

        if ($user) {

            $requestPlace = json_decode($request->place);
            $place->level = $requestPlace->level;
            $place->name = $requestPlace->name;
            $place->url = $requestPlace->url;
            $place->description = $requestPlace->description ?? null;
            $place->latitude = $requestPlace->latitude ?? null;
            $place->longitude = $requestPlace->longitude ?? null;
            $place->parent_country = $requestPlace->parent_country ?? null;
            $place->parent_state = $requestPlace->parent_state ?? null;

            if($request->hasFile('image')){
                \Cloudder::upload($request->file('image'));
                $cloudinaryResponse = \Cloudder::getResult(); 
                
                $place->thumbnail_url = $cloudinaryResponse['url'];
            }
            $place->save();
            
            $response = ["error" => false, "data" => "Place created successfully"];
            return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
        }
    }

    /**
     * Admin get the place
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPlace($id)
    {
        $place = Place::where('id', $id)->with(['country', 'state'])->first();
        
        $response = ["error" => false, "data" => $place];
        return response(json_encode($response), 200, ["Content-Type" => "application/json"]);
    }
}
