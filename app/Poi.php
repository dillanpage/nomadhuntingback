<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Tag;
use App\DetailPoi;
use App\Media;
use App\Review;

class Poi extends Model
{
    protected $fillable = ['name', 'sygic_id', 'name_local', 'name_en', 'url', 'latitude', 'longitude', 'rating', 'duration_estimate', 'description',
                            'thumbnail_url', 'image_url'];

    // Pivot table is category_poi
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // Pivot table is category_poi
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // Pivot table is poi_user
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    // Pivot table is poi_travel
    public function travels()
    {
        return $this->belongsToMany(Travel::class);
    }

    // Get the place where this POI belongs
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    /**
     * Get the details for the POI.
     */
    public function details_poi()
    {
        return $this->hasMany(DetailPoi::class);
    }

    /**
     * Get the media for the POI.
     */
    public function media()
    {
        return $this->hasMany(Media::class);
    }

    /**
     * Get the media for the POI.
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}