<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Poi;

class Tag extends Model
{
    protected $fillable = ['name', 'name_en', 'url', 'count'];

    // Pivot table is category_poi
    public function pois()
    {
        return $this->belongsToMany(Poi::class);
    }
}