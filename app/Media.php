<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = "media";

    protected $fillable = ['url'];

    public function poi()
    {
        return $this->belongsTo(Poi::class);
    }

    public function review()
    {
        return $this->belongsTo(Review::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
